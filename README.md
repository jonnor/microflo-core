microflo-core
===============
[![Build Status](https://travis-ci.org/microflo/microflo-core.png?branch=master)](https://travis-ci.org/microflo/microflo-core)

Core components for [MicroFlo](http://github.com/microflo/microflo).
